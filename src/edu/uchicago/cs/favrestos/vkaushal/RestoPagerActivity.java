package edu.uchicago.cs.favrestos.vkaushal;

import java.util.ArrayList;
import java.util.UUID;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import android.support.v4.view.ViewPager;

import edu.uchicago.cs.favrestos.vkaushal.R;

public class RestoPagerActivity extends FragmentActivity implements RestoFragment.Callbacks {
    ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.viewPager);
        setContentView(mViewPager);

        final ArrayList<Resto> restos = RestaurantHome.get(this).getRestos();

        FragmentManager fm = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public int getCount() {
                return restos.size();
            }
            @Override
            public Fragment getItem(int pos) {
                UUID crimeId =  restos.get(pos).getId();
                return RestoFragment.newInstance(crimeId);
            }
        }); 

        UUID restoID = (UUID)getIntent().getSerializableExtra(RestoFragment.EXTRA_RESTO_ID);
        for (int i = 0; i < restos.size(); i++) {
            if (restos.get(i).getId().equals(restoID)) {
                mViewPager.setCurrentItem(i);
                break;
            } 
        }
    }

    public void onRestoUpdated(Resto resto) {
        // do nothing        
    }
}
