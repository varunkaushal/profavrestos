package edu.uchicago.cs.favrestos.vkaushal;

import java.util.ArrayList;
import java.util.UUID;

import android.content.Context;

import android.util.Log;

import edu.uchicago.cs.favrestos.vkaushal.db.RestosDataSource;

public class RestaurantHome {
    private static final String TAG = "RestaurantHome";
    private static final String FILENAME = "crimes.json";

    private ArrayList<Resto> mRestos;

    private static RestaurantHome sRestaurantHome;

    private static RestosDataSource mRestosDataSource;

    private Context mAppContext;

    private RestaurantHome(Context appContext) {
        mAppContext = appContext;

        try {
            mRestos = mRestosDataSource.getAllRestos();
        } catch (Exception e) {
            mRestos = new ArrayList<Resto>();
            Log.e(TAG, "Error loading restos: ", e);
        }
    }

    public static RestaurantHome get(Context c) {
        if (sRestaurantHome == null) {
            sRestaurantHome = new RestaurantHome(c.getApplicationContext());
        }

        if(mRestosDataSource == null) {
            mRestosDataSource = new RestosDataSource(c.getApplicationContext());
        }
        return sRestaurantHome;
    }

    public Resto getResto(UUID id) {
        for (Resto c : mRestos) {
            if (c.getId().equals(id))
                return c;
        }
        return null;
    }
    
    public void addResto(Resto c) {
        mRestos.add(c);
        saveRestos();
    }

    public ArrayList<Resto> getRestos() {
        return mRestos;
    }

    public void deleteResto(Resto c) {
        mRestos.remove(c);
        saveRestos();
    }

    public boolean saveRestos() {
        try {
            //TODO Save restos to DB
//            mSerializer.saveCrimes(mRestos);
            Log.d(TAG, "restos saved to db");
            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error saving restos: " + e);
            return false;
        }
    }
}

