package edu.uchicago.cs.favrestos.vkaushal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class RestoListFragment extends ListFragment {
    private ArrayList<Resto> mRestos;
    private Callbacks mCallbacks;

    public interface Callbacks {
        void onRestoSelected(Resto resto);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public void updateUI() {
        ((RestoAdapter)getListAdapter()).notifyDataSetChanged();

        //TODO Update list to refresh names
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.restos_title);
        mRestos = RestaurantHome.get(getActivity()).getRestos();
        RestoAdapter adapter = new RestoAdapter(mRestos);
        setListAdapter(adapter);
        setRetainInstance(true);
    }

    @TargetApi(11)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        
        ListView listView = (ListView)v.findViewById(android.R.id.list);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            registerForContextMenu(listView);
        } else {
            listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            listView.setMultiChoiceModeListener(new MultiChoiceModeListener() {

                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    MenuInflater inflater = mode.getMenuInflater();

                    inflater.inflate(R.menu.resto_list_item_context, menu);
                    return true;
                }

                public void onItemCheckedStateChanged(ActionMode mode, int position,
                        long id, boolean checked) {
                }

                //TODO Update Menu Options
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_item_delete_resto:
                            RestoAdapter adapter = (RestoAdapter)getListAdapter();
                            RestaurantHome restaurantHome = RestaurantHome.get(getActivity());
                            for (int i = adapter.getCount() - 1; i >= 0; i--) {
                                if (getListView().isItemChecked(i)) {
                                    restaurantHome.deleteResto(adapter.getItem(i));
                                }
                            }
                            mode.finish();
                            adapter.notifyDataSetChanged();
                            return true;
                        default:
                            return false;
                    }
                }

                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false;
                }

                public void onDestroyActionMode(ActionMode mode) {

                }
            });

        }

        return v;
    }
    
    public void onListItemClick(ListView l, View v, int position, long id) {
        // get the Resto from the adapter
        Resto c = ((RestoAdapter)getListAdapter()).getItem(position);
        mCallbacks.onRestoSelected(c);
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ((RestoAdapter)getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.fragment_resto_list, menu);

    }

    @TargetApi(11)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.menu_item_new_resto:
                Resto resto = new Resto();
                RestaurantHome.get(getActivity()).addResto(resto);
                ((RestoAdapter)getListAdapter()).notifyDataSetChanged();
                mCallbacks.onRestoSelected(resto);
                return true;
            case R.id.menu_item_exit:
                getActivity().finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        } 
    }

    
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
        getActivity().getMenuInflater().inflate(R.menu.resto_list_item_context, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo)item.getMenuInfo();
        int position = info.position;
        RestoAdapter adapter = (RestoAdapter)getListAdapter();
        Resto resto = adapter.getItem(position);

        switch (item.getItemId()) {
            case R.id.menu_item_delete_resto:
                RestaurantHome.get(getActivity()).deleteResto(resto);
                adapter.notifyDataSetChanged();
                return true;
        }
        return super.onContextItemSelected(item);
    }

    private class RestoAdapter extends ArrayAdapter<Resto> {
        public RestoAdapter(ArrayList<Resto> restos) {
            super(getActivity(), android.R.layout.simple_list_item_1, restos);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // if we weren't given a view, inflate one
            if (null == convertView) {
                convertView = getActivity().getLayoutInflater()
                    .inflate(R.layout.list_item_resto, null);
            }

            // configure the view for this Resto
            Resto c = getItem(position);

            TextView titleTextView =
                (TextView)convertView.findViewById(R.id.resto_list_name);
            titleTextView.setText(c.getTitle());

            TextView cityTextView =
                    (TextView)convertView.findViewById(R.id.resto_list_city);
            cityTextView.setText(c.getCity());

            return convertView;
        }
    }
}

