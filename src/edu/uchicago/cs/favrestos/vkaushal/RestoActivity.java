package edu.uchicago.cs.favrestos.vkaushal;

import java.util.UUID;

import android.support.v4.app.Fragment;

public class RestoActivity extends SingleFragmentActivity {
	@Override
    protected Fragment createFragment() {
        UUID restoId = (UUID)getIntent()
            .getSerializableExtra(RestoFragment.EXTRA_RESTO_ID);
        return RestoFragment.newInstance(restoId);
    }
}
