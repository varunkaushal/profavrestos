package edu.uchicago.cs.favrestos.vkaushal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.UUID;

public class RestoFragment extends Fragment {
    public static final String EXTRA_RESTO_ID = "favrestos.RESTO_ID";
    private static final String DIALOG_DATE = "date";
    private static final String DIALOG_IMAGE = "image";
//    private static final int REQUEST_DATE = 0;
    private static final int REQUEST_PHOTO = 1;
//    private static final int REQUEST_CONTACT = 2;


    Resto mResto;
    EditText mTitleField;
    EditText mCityField;
    EditText mPhoneField;
    EditText mLatLongField;
    EditText mYelpField;
    EditText mAddressField;

    Button mDialButton;
    Button mMapButton;
    Button mGoButton;
    Button mYelpButton;
    Button mCancelButton;
    Button mSaveButton;

    Callbacks mCallbacks;

    public interface Callbacks {
        void onRestoUpdated(Resto resto);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = (Callbacks)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public static RestoFragment newInstance(UUID restoId) {
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_RESTO_ID, restoId);

        RestoFragment fragment = new RestoFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        UUID restoId = (UUID)getArguments().getSerializable(EXTRA_RESTO_ID);
        mResto = RestaurantHome.get(getActivity()).getResto(restoId);

        setHasOptionsMenu(true);
    }


    @Override
    @TargetApi(11)
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_resto, parent, false);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        
        mTitleField = (EditText)v.findViewById(R.id.resto_name);
        mTitleField.setText(mResto.getTitle());

        mCityField = (EditText)v.findViewById(R.id.resto_city);
        mCityField.setText(mResto.getCity());

        mPhoneField = (EditText)v.findViewById(R.id.resto_phone);
        mPhoneField.setText(mResto.getPhone());

        mLatLongField = (EditText)v.findViewById(R.id.resto_lat_long);
        mLatLongField.setText(mResto.getLat());

        mYelpField = (EditText)v.findViewById(R.id.resto_yelp);
        mYelpField.setText(mResto.getUrl());

        mAddressField = (EditText)v.findViewById(R.id.resto_address);
        mAddressField.setText(mResto.getAddress());

        mDialButton = (Button)v.findViewById(R.id.resto_dial);
        mDialButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Open phone to call restaurant
                Intent i = new Intent();
                i.setAction(android.content.Intent.ACTION_CALL).setData(Uri.parse("tel:"+mPhoneField.getText().toString()));
                startActivity(i);
            }
        });

        mMapButton = (Button)v.findViewById(R.id.resto_map);
        mMapButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            }
        });

        mGoButton = (Button)v.findViewById(R.id.resto_go);
        mGoButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Open Browser to go to yelp page
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(mYelpField.getText().toString()));
                startActivity(i);
            }
        });

        mYelpButton = (Button)v.findViewById(R.id.resto_extract);
        mYelpButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

            }
        });

        mCancelButton = (Button)v.findViewById(R.id.resto_cancel);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        mSaveButton = (Button)v.findViewById(R.id.resto_save);
        mSaveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mResto.setTitle(mTitleField.getText().toString());
                mResto.setCity(mCityField.getText().toString());
                mResto.setPhone(mPhoneField.getText().toString());
                mResto.setLat(mLatLongField.getText().toString());
                mResto.setAddress(mAddressField.getText().toString());
                mResto.setUrl(mYelpField.getText().toString());

                mCallbacks.onRestoUpdated(mResto);
            }
        });


        
        // if camera is not available, disable camera functionality
//        PackageManager pm = getActivity().getPackageManager();
//        if (!pm.hasSystemFeature(PackageManager.FEATURE_CAMERA) &&
//                !pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
//        }

        
        return v; 
    }
    
    private void showPhoto() {
//        // (re)set the image button's image based on our photo
//        Photo p = mResto.getPhoto();
//        BitmapDrawable b = null;
//        //TODO update to get photo from object/db
//        if (p != null) {
//            String path = getActivity()
//                .getFileStreamPath(p.getFilename()).getAbsolutePath();
//            b = PictureUtils.getScaledDrawable(getActivity(), path);
//        }
//        mPhotoView.setImageDrawable(b);
    }

    @Override
    public void onStop() {
        super.onStop();
//        PictureUtils.cleanImageView(mPhotoView);
    }

    @Override
    public void onStart() {
        super.onStart();
//        showPhoto();
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        if (requestCode == REQUEST_PHOTO) {
            // create a new Photo object and attach it to the resto
            String filename = data
                .getStringExtra(CameraFragment.EXTRA_PHOTO_FILENAME);
            if (filename != null) {
                Photo p = new Photo(filename);
                mResto.setPhoto(p);
                mCallbacks.onRestoUpdated(mResto);
                showPhoto();
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        RestaurantHome.get(getActivity()).saveRestos();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(getActivity());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        } 
    }
}
