package edu.uchicago.cs.favrestos.vkaushal;

import java.util.Date;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
//model for restos
public class Resto {

    private static final String JSON_ID = "id";
    private static final String JSON_TITLE = "title";
    private static final String JSON_PHOTO = "photo";
    
    private UUID mId;
    private String mName;
    private String mCity;
    private String mPhone;
    private String mLat;
    private String mAddress;
    private String mUrl;
    private String mNotes;
    private Photo mPhoto;
    
    public Resto() {
        mId = UUID.randomUUID();
        mName = "";
        mCity = "";
        mPhone = "";
        mLat = "";
        mAddress = "";
        mUrl = "";
        mNotes = "";
    }

    //TODO Update to SQL DB
    public Resto(JSONObject json) throws JSONException {
        mId = UUID.fromString(json.getString(JSON_ID));
        mName = json.getString(JSON_TITLE);
        if (json.has(JSON_PHOTO))
            mPhoto = new Photo(json.getJSONObject(JSON_PHOTO));
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put(JSON_ID, mId.toString());
        json.put(JSON_TITLE, mName);
        if (mPhoto != null)
            json.put(JSON_PHOTO, mPhoto.toJSON());
        return json;
    }

    @Override
    public String toString() {
        return mName;
    }

    public String getTitle() {
        return mName;
    }

    public void setTitle(String title) {
        mName = title;
    }

    public UUID getId() {
        return mId;
    }

    
    public Photo getPhoto() {
        return mPhoto;
    }

    public void setPhoto(Photo p) {
        mPhoto = p;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String mCity) {
        this.mCity = mCity;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getLat() {
        return mLat;
    }

    public void setLat(String mLat) {
        this.mLat = mLat;
    }


    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String mNotes) {
        this.mNotes = mNotes;
    }

}
