package edu.uchicago.cs.favrestos.vkaushal.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//********************************
//This is a utility class that defines some constants
//and overrides two methods
//********************************
public class RestosSQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_RESTOS = "restos"; //the name of the table
    public static final String COLUMN_ID = "_id";  //name
	public static final String COLUMN_NAME = "name";  //name
	public static final String COLUMN_CITY = "city"; //column
	public static final String COLUMN_PHONE = "phone"; //column
    public static final String COLUMN_ADDRESS = "address"; //column
    public static final String COLUMN_URL = "url"; //column
    public static final String COLUMN_LATLONG = "lat_long"; //column

	private static final String DATABASE_NAME = "restos_db.db"; //the db name
	private static final int DATABASE_VERSION = 2; //the version. to upgrade (when schema changes) simply increment the version and it'll blow away existing data.

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_RESTOS + "("
            + COLUMN_ID	+ " integer primary key autoincrement, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_CITY + " text not null, "
	        + COLUMN_PHONE + " text not null, "
            + COLUMN_ADDRESS + " text not null, "
            + COLUMN_URL + " text not null, "
            + COLUMN_LATLONG + " text not null);" ;

	//constructor
	public RestosSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	//this class has an onCreate method that we need to override, and pass in the create-string
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	// to upgrade, simply change the db_version
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.wtf(RestosSQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESTOS);
		onCreate(db);
	}

} 