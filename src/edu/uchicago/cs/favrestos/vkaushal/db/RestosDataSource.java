package edu.uchicago.cs.favrestos.vkaushal.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import edu.uchicago.cs.favrestos.vkaushal.Resto;

public class RestosDataSource {

	private static String TAG = "Restos";
	// Database fields
	private SQLiteDatabase mSQLiteDatabase;
	private RestosSQLiteHelper mRestosSQLiteHelper;
	private String[] mColumns = { RestosSQLiteHelper.COLUMN_ID,
			RestosSQLiteHelper.COLUMN_NAME, RestosSQLiteHelper.COLUMN_CITY,
            RestosSQLiteHelper.COLUMN_PHONE, RestosSQLiteHelper.COLUMN_ADDRESS,
            RestosSQLiteHelper.COLUMN_URL, RestosSQLiteHelper.COLUMN_LATLONG,};

	public RestosDataSource(Context context) {
		mRestosSQLiteHelper = new RestosSQLiteHelper(context);
	}

	public void open() throws SQLException {
		mSQLiteDatabase = mRestosSQLiteHelper.getWritableDatabase();
	}

	public void close() {
		mRestosSQLiteHelper.close();
	}
	
	public boolean isOpen(){
		return (mSQLiteDatabase != null);
	}

	// basic CRUD operations
	// CREATE
	public Resto createResto(String strName, String strCity, String strPhone, String strLatLong, String strURL, String strAddress) {
		ContentValues values = new ContentValues();
		values.put(RestosSQLiteHelper.COLUMN_NAME, strName);
		values.put(RestosSQLiteHelper.COLUMN_CITY, strCity);
        values.put(RestosSQLiteHelper.COLUMN_PHONE, strPhone);
        values.put(RestosSQLiteHelper.COLUMN_LATLONG, strLatLong);
        values.put(RestosSQLiteHelper.COLUMN_URL, strURL);
        values.put(RestosSQLiteHelper.COLUMN_ADDRESS, strAddress);
		long insertId = mSQLiteDatabase.insert(RestosSQLiteHelper.TABLE_RESTOS, null,
				values);
		Cursor cursor = mSQLiteDatabase.query(RestosSQLiteHelper.TABLE_RESTOS,
                mColumns, RestosSQLiteHelper.COLUMN_ID + " = " + insertId, null,
				null, null, null);
		cursor.moveToFirst();
		Resto newResto = cursorToResto(cursor);
		cursor.close();
		return newResto;
	}

	// READ
	public ArrayList<Resto> getAllRestos() {
		ArrayList<Resto> restos = new ArrayList<Resto>();

		Cursor cursor = mSQLiteDatabase.query(RestosSQLiteHelper.TABLE_RESTOS,
                mColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Resto resto = cursorToResto(cursor);
			restos.add(resto);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return restos;
	}

	// get one Todo
	// UPDATE
	public int updateResto(Resto com) {

		ContentValues cv = new ContentValues();

		cv.put(RestosSQLiteHelper.COLUMN_NAME, com.getTitle());
		cv.put(RestosSQLiteHelper.COLUMN_CITY, com.getCity());
        cv.put(RestosSQLiteHelper.COLUMN_PHONE, com.getPhone());
        cv.put(RestosSQLiteHelper.COLUMN_LATLONG, com.getLat());
        cv.put(RestosSQLiteHelper.COLUMN_URL, com.getUrl());
        cv.put(RestosSQLiteHelper.COLUMN_ADDRESS, com.getAddress());

		Log.i(TAG, "Todo updated with id: " + com.getId());

		return mSQLiteDatabase.update(RestosSQLiteHelper.TABLE_RESTOS, cv,
				RestosSQLiteHelper.COLUMN_ID + " =?",
				new String[] { String.valueOf(com.getId()) });

	}

	// DELETE
	public void deleteResto(Resto resto) {
		String name = resto.getTitle();

		mSQLiteDatabase.delete(RestosSQLiteHelper.TABLE_RESTOS,
                RestosSQLiteHelper.COLUMN_NAME + " = " + name, null);
		Log.i(TAG, "Resto deleted with name: " + name);
	}
	
	//overriden to take an id
	public void deleteResto(long lId) {
		mSQLiteDatabase.delete(RestosSQLiteHelper.TABLE_RESTOS,
                RestosSQLiteHelper.COLUMN_ID + " = " + lId, null);
		Log.i(TAG, "Todo deleted with id: " + lId);
	}


	private Resto cursorToResto(Cursor cursor) {
        Resto resto = new Resto();
        resto.setTitle(cursor.getString(1));
        resto.setCity(cursor.getString(2));
        resto.setPhone(cursor.getString(3));
        resto.setAddress(cursor.getString(4));
        resto.setUrl(cursor.getString(5));
        resto.setLat(cursor.getString(6));
		return resto;
	}
}